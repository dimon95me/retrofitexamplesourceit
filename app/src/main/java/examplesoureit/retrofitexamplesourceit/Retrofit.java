package examplesoureit.retrofitexamplesourceit;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public class Retrofit {

    private static final String ENDPOINT = "http://restcountries.eu/rest";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    private static void initialize(){
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    interface ApiInterface{
        @GET("/v2/name/{name}")
        void getCountries(@Path("name") String countryName,
                          @Query("fulltext") String query,
                          Callback<List<Country>> callback);
    }

    public static void getCountries(String name, Callback<List<Country>> callback){
        apiInterface.getCountries(name, "true", callback);
    }

}
