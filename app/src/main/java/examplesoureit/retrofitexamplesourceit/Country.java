package examplesoureit.retrofitexamplesourceit;

import com.google.gson.annotations.SerializedName;

class Country {

    @SerializedName("name")
    public String name;
    @SerializedName("capital")
    public String capital;
    @SerializedName("population")
    public String population;
}
